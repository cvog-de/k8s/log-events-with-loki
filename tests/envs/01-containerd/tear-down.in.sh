cd "${env_dir}"

container stop --time 0 "${promtail_container_name}" || true

container stop --time 0 "${loki_container_name}" || true

container network rm testing-network || true

rm -f \
  "${promtail_config_mount}" \
  "${promtail_sample_mount}" \
  "${promtail_stdout_mount}" \
  "${promtail_stderr_mount}"
