cd "${env_dir}"

touch \
  "${promtail_config_mount}" \
  "${promtail_sample_mount}" \
  "${promtail_stdout_mount}" \
  "${promtail_stderr_mount}"

container network create -d bridge testing-network || true

container run --detach --rm \
  --name "${loki_container_name}" \
  --network testing-network \
  --network-alias loki \
  --publish '127.0.0.1:3100:3100/tcp' \
  "${loki_container_image}" || true

container run --detach --rm \
  --name "${promtail_container_name}" \
  --network testing-network \
  --network-alias promtail \
  --read-only --volume \
      "./${promtail_config_mount}:/etc/promtail/config.yaml" \
  --read-only --volume "./${promtail_sample_mount}:/sample.log" \
  --volume "./${promtail_stdout_mount}:/stdout.log" \
  --volume "./${promtail_stderr_mount}:/stderr.log" \
  --entrypoint '/bin/bash' \
  "${promtail_container_image}" \
  '-c' 'sleep infinity' || true

