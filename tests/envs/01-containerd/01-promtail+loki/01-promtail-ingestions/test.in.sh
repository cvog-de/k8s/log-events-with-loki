cd "${sample_dir}"

cat "promtail.conf.yaml" > "${env_dir}/${promtail_config_mount}"
cat "input.json.log"     > "${env_dir}/${promtail_sample_mount}"

script="$( str \
   'set -euo pipefail ; cat /sample.log | promtail '   \
      '-config.file=/etc/promtail/config.yaml '        \
      '--dry-run --stdin > /stdout.log 2> /stderr.log' )"

if container exec "${promtail_container_name}" \
       /bin/bash -c "${script}"
  then
    sed -re '1,/^$/d' \
      < "${env_dir}/${promtail_stdout_mount}" \
      | tr '\t' ' ' \
      | sort \
      > "result.promtail.log"
      
    assert equal-files \
      "assert.promtail.log" \
      "result.promtail.log"

    #rm -f "result.promtail.log"
      
    true
  else
    error "while executing script in container:"
    cat "${env_dir}/${promtail_stderr_mount}" | indent

    false
  fi
