# Log Events with Loki ########################################################

Developers wish to get Kubernetes Events logged for deeper insights in the
executing context of their containers. However, as far as I know, there is no
predetermined way to do so and that's why we brew our own solution.
